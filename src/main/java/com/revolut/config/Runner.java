package com.revolut.config;

import com.revolut.verticles.AppServerVerticle;

public class Runner {

  public static void main(String[] args) {
    AppServerVerticle as = new AppServerVerticle();
    try {
      as.start();

    } catch (Exception e) {
      System.out.println("something wrong");
    }
  }

}
